#include <iostream>
#define MAX 10

using namespace std;

int main()
{
    int tab[MAX][MAX];
    for (int i=0; i<MAX; i++)
      for (int j=0; j<MAX; j++) {
        if (i == j)
          tab[i][j] = 1;
        else tab[i][j] = 0;
        if (i+j == MAX-1)
          tab[i][j] = 1;
      }

    for (int i=0; i<MAX; i++) {
      for (int j=0; j<MAX; j++)
        cout << tab[i][j] << " ";
      cout << endl;
    }
    return 0;
}
