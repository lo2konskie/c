#include <iostream>

using namespace std;

struct element {
   int zawartosc;
   element* nastepny;
} *poczatek, *koniec;

void do_kolejki(int liczba) {
    element* pom;
    pom = new element;
    pom->zawartosc = liczba;
    pom->nastepny = NULL;
    if (poczatek)
      koniec->nastepny = pom;
    else
      poczatek = pom;
    koniec = pom;
}

int z_kolejki() {
    element *pom;
    int zawartosc = poczatek->zawartosc;
    pom = poczatek->nastepny;
    delete poczatek;
    poczatek = pom;
    return zawartosc;
}

int main() {
    for (int i=0; i<100; i++)
      do_kolejki(i);
    while (poczatek)
      cout << z_kolejki() << endl;
}
