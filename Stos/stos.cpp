#include <iostream>

using namespace std;

struct element {
   int zawartosc;
   element* nastepny;
} *wierzcholek;

void na_stos(int liczba) {
    element* pom;
    pom = new element;
    pom->zawartosc = liczba;
    pom->nastepny = wierzcholek;
    wierzcholek = pom;
}

int ze_stos() {
    element *pom;
    int zawartosc = wierzcholek->zawartosc;
    pom = wierzcholek->nastepny;
    delete wierzcholek;
    wierzcholek = pom;
    return zawartosc;
}

int main() {
    for (int i=0; i<100; i++)
      na_stos(i);
    int i;
    while (wierzcholek) {
      i = ze_stos();
      cout << i << endl;
    }
}
